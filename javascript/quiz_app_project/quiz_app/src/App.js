import logo from "./logo.svg";
import "./App.css";
import React, { useState, useEffect } from "react";
import Question from "./Question";
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Start from './Start';

function App() {
  const [question_data, setQuestion] = useState({
    question: "no question yet",
    incorrect_answers: [],
    corr_ans: "",
  });

  const [points, setPoints] = useState(0);

  const [start, setStart] = useState(false);

  function handle_question(ques_json) {
    const question_data_from_api = ques_json.results[0];
    console.log(question_data_from_api.correct_answer.replaceAll('&quot;', '"'));
    return {
      question: question_data_from_api.question.replaceAll('&quot;', '"'),
      incorrect_answers: question_data_from_api.incorrect_answers.map((data) => data.replaceAll('&quot;', '"')),
      corr_ans: question_data_from_api.correct_answer.replaceAll('&quot;', '"'),
    };
  }

  const fetch_question = () => {
    fetch("https://opentdb.com/api.php?amount=1&type=multiple")
    .then((resp) => resp.json())
    .then((data) => setQuestion(handle_question(data)));

  };

  useEffect(() => {
    fetch_question();
  }, [])

  const addPoint = () => setPoints(points + 1);

  const startGame = () => setStart(true);

  return (
    <div className="App">
      {console.log(start)}
      {start ? <><Question question_data={question_data} call_back={addPoint} next={fetch_question} />
      <br></br>
      <br></br>
      <Button onClick={fetch_question}>next</Button>
      <h3>you have : {points} points</h3> </> : <Start call_back={startGame}></Start>}
      
    </div>
  );
}

export default App;
