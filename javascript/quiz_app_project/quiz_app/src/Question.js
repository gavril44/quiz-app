import { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

function Question(props) {
  var answers = props.question_data.incorrect_answers.slice();
  var test = answers.splice(
    Math.floor(Math.random() * 4),
    0,
    props.question_data.corr_ans
  );

  const checkAnswerB1 = () => {
    if (answers[0] === props.question_data.corr_ans) {
      props.call_back();
      props.next();
    }
    else {
      props.next();
    }
  };
  const checkAnswerB2 = () => {
    if (answers[1] === props.question_data.corr_ans) {
      props.call_back();
      props.next();
    }
    else {
      props.next();
    }
  };

  const checkAnswerB3 = () => {
    if (answers[2] === props.question_data.corr_ans) {
      props.call_back();
      props.next();
    }
    else {
      props.next();
    }
  };

  const checkAnswerB4 = () => {
    if (answers[3] === props.question_data.corr_ans) {
      props.call_back();
      props.next();
    }
    else {
      props.next();
    }
  }

  return (
    <Container>
      <Row>
        <Col lg="auto"></Col>
        <Col>
          <h2>{props.question_data.question}</h2>
        </Col>
      </Row>
      <br></br>
      <Row>
        <Col>
          <Button onClick={checkAnswerB1}>{answers[0]}</Button>
        </Col>
        <Col>
          <Button onClick={checkAnswerB2}>{answers[1]}</Button>
        </Col>
      </Row>
      <br></br>
      <Row>
        <Col>
          <Button onClick={checkAnswerB3}>{answers[2]}</Button>
        </Col>
        <Col>
          <Button onClick={checkAnswerB4}>{answers[3]}</Button>
        </Col>
      </Row>
    </Container>
  );
}

export default Question;
