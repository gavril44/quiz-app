import { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";


export default function Start(props) {
    return (
        <Container>
            <h1>Hello! welcome to the gavri trivia game.</h1>
            <h1>would you like to start the game?</h1>
            <br></br>

            <Button onClick={() => props.call_back()}>Press to start</Button>
        </Container>
    );
}